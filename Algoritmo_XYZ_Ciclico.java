/**
 * @Diego Cajas
 */

public class Algoritmo_XYZ_Ciclico {
    
    public static void main(String[] args) {
        
        System.out.println( "Fibonacci con metodo Ciclico");
        //Llamada al metodo ciclico con un valor de 50
        serieFibonacciCiclico.fibonacciCiclico(50);
    }
}

//Clase de la serie Fibonacci
class serieFibonacciCiclico {
    
    //Metodo sin retorno para imprimir la serie
    public static void fibonacciCiclico( long numero ){
        
        //Declaracion de variables
        long siguiente = 1, actual = 0, temporal = 0;
        //Uso del ciclo iterativo for para la impresion de datos
        for (long x = 0; x <= numero; x++) {
            System.out.printf("Fibonacci de %d es: %d\n",x,actual);
            temporal = actual;
            actual = siguiente;
            siguiente = siguiente + temporal;
        }
    } // fin del método fibonacci ciclico
}