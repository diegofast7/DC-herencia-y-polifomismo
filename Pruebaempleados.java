public class Pruebaempleados {

    public static void main(String[] args) {
        
        Secretario secretario1 = new Secretario(
        "Sue","Jones","222-22-2222","Mindo",10,"0934873233",400,0);
        
        Coche coche1 = new Coche("PGH-324","Toyota","Todoterreno");
        Vendedor vendedor1 = new Vendedor(
        "Miles","Jones","222-22-2222","Mindo",1,"022690765",400,1,"44453-9986",coche1,"093476345","Sur de Riobamba");
        
        Coche coche2 = new Coche("PGG-564","Kia","Sedan");
        Jefe jefe1 = new Jefe(
        "Miles","Jones","222-22-2222","Mindo",1,"0934873233",400,1,"D04",coche2);

        Secretario.imprimirDatos(secretario1);
        
        Vendedor.imprimirDatos(vendedor1);
        
        Jefe.imprimirDatos(jefe1);
    }
}

abstract class Empleado {

    private String nombres;
    private String apellidos;
    private String dni;
    private String direccion;
    private int anios;
    private String telefono;
    private double salarioBase;
    private int puesto;
    private String supervisor;
    private double tarifa;
    private double salario;

    public Empleado(String nombre, String apellido, String nss, String dirc,
            int time,String telf, double sal, int cargo){
        //Constructor de la clase abstracta
        this.nombres=nombre;
        this.apellidos=apellido;
        this.dni=nss;
        this.direccion=dirc;
        this.anios=time;
        this.telefono=telf;
        this.salarioBase=sal;
        establecerTarifa(time,cargo);
        establecerSalario(sal);
    }

    public String  obtenerNombre(){

        return nombres;
    }

    public String  obtenerApellidos(){

        return apellidos;
    }

    public String  obtenerDNI(){

        return dni;
    }
    
    public String  obtenerDireccion(){

        return direccion;
    }
    
    public int  obtenerAnios(){

        return anios;
    }
    
    public String obtenerTelefono (){

        return telefono;
    }
    
    public double obtenerSalarioBase(){

        return salarioBase;
    }
    
    public double obtenerPuesto(){

        return puesto;
    }
    
    
    public void establecerTarifa(int anio, int puest){
        if(anio>0 && puest==0){
            tarifa=(obtenerSalarioBase()*0.80)*anio;
        }  
        else if(anio>0 && puest==1){
            tarifa=(obtenerSalarioBase()*0.10)*anio;
        }  
        else if(anio>0 && puest==2){
            tarifa=(obtenerSalarioBase()*0.20)*anio;
        }
        else{
            tarifa=0;
        }
    }
    public double obtenerTarifa(){
        
        return tarifa;
    }
    
    public void establecerSalario(double saldo){
        
        if(saldo>=0.0&&tarifa>0.1){
            salario=obtenerSalarioBase()+obtenerTarifa();
        }
        else if(saldo>=0.0&&tarifa==0){
            salario=obtenerSalarioBase();
        }
        else
            throw new IllegalArgumentException(
            "El salario base debe ser >= 0.0");
    }
    public double obtenerSalario(){
        
        return salario;
    }

    @Override
    public String toString(){

        return String.format("Nombre: %s %s \nNumero de seguro social: %s\n"
                + "Direccion: %s",
                obtenerNombre(),obtenerApellidos(),obtenerDNI(),
                obtenerDireccion());
    }
}//Fin de la clase abstracta Empleado

//clase Secretario
class Secretario extends Empleado{
    int cargo=0;

    public Secretario(String nombre, String apellido, String nss, String dirc,
            int time,String telf, double sal, int cargo){
        //Constructor de la clase abstracta
        super(nombre, apellido, nss, dirc, time
               , telf, sal, cargo);
    }
    
    public static void imprimirDatos(Secretario emp){
        System.out.printf("\n%s:\n\n%s\n\n", "Informacion del Empleado",emp);
    }
    
    @Override
    public String toString(){

        return String.format("Nombre: %s %s \nNumero de seguro social: %s\n"
                + "Direccion: %s\nPuesto: Secretario",
                obtenerNombre(),obtenerApellidos(),obtenerDNI(),
                obtenerDireccion());
    }
}//Fin de la clase Secretario

//clase Vendedor
class Vendedor extends Empleado{

    private String fax;
    private Coche coche;
    private String movil;
    private String area;

    public Vendedor(String nombre, String apellido, String nss, String dirc,
            int time,String telf, double sal, int cargo, String fa, Coche carro, String mov, String are){
        //Constructor de la clase abstracta
        super(nombre, apellido, nss, dirc, time
               , telf, sal, cargo);
        fax=fa;
        coche=carro;
        movil=mov;
        area=are;
    }
    
    public static void imprimirDatos(Vendedor emp){
        
        System.out.printf("\n%s:\n\n%s\n\n", "Informacion del Empleado",emp);
    }
    
    public void darDeAltaCliente(){
        
    }
    
    public void darDeBajaCliente(){
        
    }
    
    public void cambiarCoche(){
        
    }
    
    public String obtenerFax(){
        return fax;
    }
    
    public String obtenerMovil(){
        return movil;
    }
    
    public String obtenerArea(){
        return area;
    }

    @Override
    public String toString(){

        return String.format("Nombre: %s %s\nCedula: %s\n"
                + "Direccion: %s\nTelefono: %s\nFax: %s\nMovil; %s\nArea: %s\nPuesto: Vendedor",
                obtenerNombre(),obtenerApellidos(),obtenerDNI(),
                obtenerDireccion(),obtenerTelefono(),obtenerFax(),obtenerMovil(),obtenerArea());
    }
}//Fin de la clase Vendedor

//clase Jefe de Zona
class Jefe extends Empleado{

    private String despacho;
    Coche coche;

    public Jefe(String nombre, String apellido, String nss, String dirc,
            int time,String telf, double sal, int cargo, String desch, Coche carro){
        //Constructor de la clase abstracta
        super(nombre, apellido, nss, dirc, time
               , telf, sal, cargo);
        despacho=desch;
        coche=carro;
    }
    
    public static void imprimirDatos(Jefe emp){
        System.out.printf("\n%s:\n\n%s\n\n", "Informacion del Empleado",emp);
    }
    
    public String obtenerDespacho(){
        return despacho;
    }
    
    public void darDeAltaVendedor(){
        
    }
    
    public void darDeBajaVendedor(){
        
    }
    
    public void cambiarSecretario(){
        
    }
    
    public void cambiarCoche(){
        
    }

    @Override
    public String toString(){

        return String.format("Nombre: %s %s \nNumero de seguro social: %s\n"
                + "Direccion: %s\nDespacho N°: %s\nPuesto: Jefe de Zona",
                obtenerNombre(),obtenerApellidos(),obtenerDNI(),
                obtenerDireccion(),obtenerDespacho());
    }
}//Fin de la clase Jefe de Zona

class Coche{
    
    private String matricula;
    private String marca;
    private String modelo;
    
    public Coche(String mat, String mar, String modl){
        this.matricula = mat;
        this.marca = mar;
        this.modelo = modl;
    }
    
    public String obtenermatricula(){
        return matricula;
    }
    public String obtenermarca(){
        return marca;
    }
    public String obtenermodelo(){
        return modelo;
    }
    public static void imprimirCoche(Coche emp){
        System.out.printf("\n%s:\n\n%s\n\n", "Informacion del Coche",emp);
    }
    
    @Override
    public String toString(){

        return String.format("Matricula: %s\nMarca: %s \nModelo: %s\n",
                obtenermatricula(),obtenermarca(),obtenermodelo());
    }
}