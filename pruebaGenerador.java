import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Random;

public class pruebaGenerador {
    
    public static void main( String[] args ){
        Generador generador = new Generador();

        generador.abrirArchivo();
        generador.crearTelefonos();
    } 
}

class Generador {
    private Formatter archivo;
    
    public void abrirArchivo(){
        try{
        
            archivo = new Formatter( "Telefonos.txt" ); // abre el archivo
        } // fin de try
        catch ( SecurityException securityException ){
            
            System.err.println(
                "No tiene acceso de escritura a este archivo." );
            System.exit( 1 ); // termina el programa
        } // fin de catch
        catch ( FileNotFoundException fileNotFoundException ){
            
            System.err.println( "Error al abrir o crear el archivo." );
            System.exit( 1 ); // termina el programa
        } // fin de catch
    } // fin del método abrirArchivo
    
    public void crearTelefonos(){
        
        Scanner entrada = new Scanner( System.in );
        try{
            Generador aplicacion = new Generador();
            String[] a= aplicacion.generarNombres(generarNumerosAleaorios());
            String[] b= aplicacion.generarNombres(generarNumerosAleaorios());
            String[] c= aplicacion.generarNombres(generarNumerosAleaorios());
            String[] d= aplicacion.generarNombres(generarNumerosAleaorios());
            String[] e= aplicacion.generarNombres(generarNumerosAleaorios());
            String[] f= aplicacion.generarNombres(generarNumerosAleaorios());
            String[] g= aplicacion.generarNombres(generarNumerosAleaorios());
	
            for(int i=0;i<a.length;i++){
		for(int j=0;j<b.length;j++){
                    for(int k=0;k<c.length;k++){
			for(int l=0;l<d.length;l++){
                            for(int m=0;m<e.length;m++){
				for(int n=0;n<f.length;n++){
                                    for(int o=0;o<g.length;o++){
                                        archivo.format( a[i]+b[j]+c[k]+d[l]+e[m]+f[n]+g[o]+"\n"); 
                                    }
                                }
                            }
                        }
                    }
                }
            }	
            archivo.close();
        }
        catch ( NoSuchElementException elementException ){
            
            System.err.println( "Entrada invalida. Intente de nuevo." );
            entrada.nextLine();
        }
    }
    
    public String[] generarNombres( int x){
        switch(x) {
            case 2:
                String a[] = new String[]{"a","b","c"};
                return a;
            case 3:
                String b[] = new String[]{"d","e","f"};
                return b;
            case 4:
                String c[] = new String[]{"g","h","i"};
                return c;
            case 5:
                String d[] = new String[]{"j","k","l"};
                return d;
            case 6:
                String e[] = new String[]{"m","n","o"};
                return e;
            case 7:
                String f[] = new String[]{"p","q","r","s"};
                return f;
            case 8:
                String g[] = new String[]{"t","u","v"};
                return g;
            case 9:
                String h[] = new String[]{"w","x","y","z"};
                return h;
            default:
                String i[] = new String[]{"","",""};
                return i;
        }
    }
    
    public int generarNumerosAleaorios( ){
        Random rand = new Random(); 
        int a = rand.nextInt(10);
        if(a>1&&a<10){
            System.out.println(a);
            return a;
        }else{
            return generarNumerosAleaorios();
        }
    }
}
